===============================================
 Contribution Guide for Adélie Linux Patch Set
===============================================
:Author:
  * **A. Wilcox**, documentation writer
:Status:
  Draft
:Copyright:
  © 2016 Adélie Linux.  NCSA open source licence.




Introduction
============

This repository contains the Adélie Linux patch set.  It is used when building
packages for official Adélie Linux repositories.

Combining upstream sources with the patches here will provide you with the
exact source code used for building a particular package.


Licenses
`````````
As the Adélie Linux project is an open-source Linux distribution, patches
contained in this repository must also be provided under a license recognised
by the OSI_.  Typically, patches must be licensed under the same terms as the
upstream package is licensed.

.. _OSI: http://opensource.org/licenses/category


Changes
```````
Any changes to this repository must be reviewed before being pushed to the
master branch.  There are no exceptions to this rule.  For security-sensitive
updates, contact the Security Team at sec-bugs@adelielinux.org.




Patch Format
============

This section describes the format required for patches in this repository.


Unifed Diff Format
``````````````````

Patches must be submitted in unifed diff format (``diff -Nu``).  There are no
exceptions to this rule.


Size
````

Patch files must be under 50 Kbyte, and under 500 lines.


Directory Level
```````````````

All patch files must be appliable using ``patch -p1``.  This means that the
``diff`` command must be run one directory above the package's root.


Comments
````````

It is highly encouraged that you use the top matter of your patch file to
describe the changes being made.  This grants other developers and the Adélie
Linux team the same level of understanding that you have over the patch set.
The format provided by ``svn`` or ``git`` is acceptable, and a direct upstream
commit number is even better.




Contributing Changes
====================

This section describes the usual flows of contribution to this repository.


GitLab Pull Requests
````````````````````

#. If you do not already have a GitLab account, you must create one.

#. Create a *fork* of the packages repository.  For more information, consult
   the GitLab online documentation.

#. Clone your forked repository to your computer.

#. Make your changes.

#. Test your changes to ensure they are correct.

#. Add (or remove) changed files using ``git add`` and ``git rm``.

#. Commit your changes to the tree using the command ``git commit`` and
   ``git push``.

#. Visit your forked repository in a Web browser.

#. Choose the *Create Pull Request* button.

#. Review your changes to ensure they are correct, and then submit the form.


Mailing List
````````````

#. Clone the packages repository to your computer.

#. Make your changes.

#. Test your changes to ensure they are correct.

#. Add (or remove) changed files using ``git add`` and ``git rm``.

#. Commit your changes to the tree using the command ``git commit``.

#. Use the command ``git format-patch HEAD^`` to create a patch file for your
   commit.

   .. note:: If you have made multiple commits to the tree, you will need to
             add an additional ^ for each commit you have made.  For example,
             if you have made three commits, you will use the command
             ``git format-patch HEAD^^^``.

#. Email the resulting patch to the packagers mailing list.
