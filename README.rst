===================================
 README for Adélie Linux Patch Set
===================================
:Authors:
  * **A. Wilcox**, primary maintainer
  * **Elizabeth Myers**, maintainer
  * **Adélie Linux Developers and Users**, contributions
:Status:
  Production
:Copyright:
  © 2016 Adélie Linux.  NCSA open source licence.




Introduction
============

This repository contains the Adélie Linux patch set.  It is used when building
packages for official Adélie Linux repositories.

Combining upstream sources with the patches here will provide you with the
exact source code used for building a particular package.


Licenses
`````````
As the Adélie Linux project is an open-source Linux distribution, patches
contained in this repository must also be provided under a license recognised
by the OSI_.  Typically, patches must be licensed under the same terms as the
upstream package is licensed.

.. _OSI: http://opensource.org/licenses/category


Changes
```````
Any changes to this repository must be reviewed before being pushed to the
master branch.  There are no exceptions to this rule.  For security-sensitive
updates, contact the Security Team at sec-bugs@adelielinux.org.




Usage
=====

This section contains usage information for this repository.


From portage++
``````````````

This repository must be checked out in the ``/etc/portage`` directory of the
Portage++ root, and must be named ``patches``.  There is no further
configuration required.


On a Gentoo computer
````````````````````

The same basic procedure applies to using this patch set on a Gentoo Linux
computer.  These patches are specifically designed for the Adélie Linux
system and have not been tested on other distributions.  They may prove useful
in certain instances, such as running the musl libc, but they are not designed
for any use outside of Adélie Linux packages.  Breakage on computers not
running Adélie is not a bug and will not be fixed.
